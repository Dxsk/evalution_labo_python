import logging

import requests

from chatserver import TRIGGER
from helpers import log


class Command:

    lvl = logging.INFO

    def __init__(self, message):
        self.message_list = message.split(' ')
        self.command = self.message_list[0]
        self.args = self.message_list.pop(0)
        self.test = 'test'

    def verify_command(self):
        log(self.lvl, '=========')
        log(self.lvl, f' --> {self.message_list}')
        log(self.lvl, f' --> {self.command}')  # <-- je ne sais pas pourquoi mais il retire '/' dedans ?
        log(self.lvl, f' --> {self.args}')
        log(self.lvl, f' --> {self.test}')
        log(self.lvl, TRIGGER in self.command)
        log(self.lvl, '=========')
        if TRIGGER in self.command:
            log(lvl=self.lvl, msg=' --> TRIGGER FOUND')
            return True

    def info_ip(self):
        r = requests.get(f'https://ipinfo.io/{self.args[0]}')
        if r.status_code == 200:
            return r.text
