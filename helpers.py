import logging


def log(lvl, msg):
    return logging.log(level=lvl, msg=msg)
