FROM python:3.9-alpine


# System
ENV PYTHONUNBUFFRED 1
ENV LANG=C.UTF-8
RUN apk update
RUN apk upgrade
RUN apk add gcc libc-dev linux-headers
RUN pip install pipenv


# localtime
RUN apk add tzdata
RUN cp /usr/share/zoneinfo/Europe/Paris /etc/localtime
RUN echo "Europe/Paris" > /etc/timezone
RUN apk del tzdata


# Add user and workdir
RUN adduser adminchatserver --disabled-password
RUN mkdir -p /code
RUN chown -R adminchatserver:adminchatserver /code
RUN chmod 750 -R /code
WORKDIR /code
