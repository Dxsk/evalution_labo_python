import logging
import socket
import threading

from chatserver import DEBUG, HEADER, SERVER_HOST, SERVER_PORT
from commands import Command
from helpers import log


class Server(threading.Thread):
    def __init__(self, host, port):
        super().__init__()
        self.connections = []
        self.host = host
        self.port = port

    def run(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((self.host, self.port))
        sock.listen(1)
        log(lvl=logging.INFO, msg=f' --> Listening connection : {sock.getsockname()}')

        while True:
            sc, sock_name = sock.accept()
            log(lvl=logging.INFO, msg=f' --> New connection : {sc.getpeername()} '
                                      f'to {sc.getsockname()}')
            server_socket = ServerSocket(sc, sock_name, self)
            server_socket.start()
            self.connections.append(server_socket)
            log(lvl=logging.INFO, msg=f' --> Listen messages : {sc.getpeername()}')

    def broadcast(self, message, source):
        for connection in self.connections:
            if connection.sock_name != source:
                connection.send(message)

    def remove_connection(self, connection):
        self.connections.remove(connection)


class ServerSocket(threading.Thread):
    def __init__(self, sock, sock_name, serv):
        super().__init__()
        self.sock = sock
        self.sock_name = sock_name
        self.serv = serv

    def run(self):
        while True:
            message = self.sock.recv(1024).decode('ascii')
            if message:
                command = Command(message=message)
                log(lvl=logging.INFO, msg=command.verify_command())
                if not command.verify_command():
                    log(lvl=logging.INFO, msg=f' --> {self.sock_name} : {message}')
                    self.serv.broadcast(message, self.sock_name)

            else:
                log(lvl=logging.INFO, msg=f' --> {self.sock_name} has closed the connection')
                self.sock.close()
                self.serv.remove_connection(self)

    def send(self, message):
        self.sock.sendall(message.encode('ascii'))


if __name__ == '__main__':

    print(HEADER)

    if DEBUG:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    log(lvl=logging.INFO, msg=' --> Start ChatServer')

    srv = Server(
        SERVER_HOST,
        int(SERVER_PORT),
    )
    srv.start()

    thread = threading.Thread(target=exit, args=(srv, ))
    thread.start()
