#!/bin/sh

# Variables
export PATHDIR='/code'
export PYENV='python -m pipenv'
export PYRUN='python -m pipenv run python'

cd $PATHDIR
$PYENV install

$PYRUN server.py
