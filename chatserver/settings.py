import socket

# Settings server
DEBUG = True
SERVER_HOST = socket.gethostname()
SERVER_PORT = 1337
TRIGGER = '/'

# Header 'fun'
HEADER = """####
#
# By Dorian Wilhelm
#   Evaluation Laboratory Python
#
# Chat with Socket
# Server side --
####"""
