import argparse
import socket
import sys
import threading
import tkinter as tk

leave_list = ['q', 'quit', 'QUIT', 'l', 'leave', 'LEAVE', 'exit']


class Send(threading.Thread):
    def __init__(self, sock, name):
        super().__init__()
        self.sock = sock
        self.name = name

    def run(self):
        while True:
            print(f'{self.name}: ', end='')
            sys.stdout.flush()
            message = sys.stdin.readline()[:-1]

            if message in leave_list:
                self.sock.sendall(f'Server: {self.name} has left the chat.'.encode('ascii'))
                self.sock.close()
                sys.exit(0)

            else:
                self.sock.sendall(f'{self.name}: {message}'.encode('ascii'))


class Receive(threading.Thread):
    def __init__(self, sock, name):
        super().__init__()
        self.sock = sock
        self.name = name
        self.messages = None

    def run(self):
        while True:
            message = self.sock.recv(1024).decode('ascii')

            if message:
                if self.messages:
                    self.messages.insert(tk.END, message)
                    print(f'\r{message}\n{self.name}: ', end='')

                else:
                    print(f'\r{message}\n{self.name}: ', end='')

            else:
                self.sock.close()

            sys.exit(0)


class Client:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.name = None
        self.messages = None

    def start(self):
        print(f'Trying to connect to {self.host}:{self.port} ...')
        self.sock.connect((self.host, self.port))
        print(f'Successfully connected to {self.host}:{self.port}')

        self.name = input('Your name: ')

        send = Send(self.sock, self.name)
        receive = Receive(self.sock, self.name)

        send.start()
        receive.start()

        self.sock.sendall(f'Server: {self.name} has joined the chat'.encode('ascii'))
        print(f"For leave the chat : {leave_list}'\n")
        print(f'{self.name}: ', end='')

        return receive

    def send(self, text_input):
        message = text_input.get()
        text_input.delete(0, tk.END)
        self.messages.insert(tk.END, f'{self.name}: {message}')

        if message in leave_list:
            self.sock.sendall(f'Server: {self.name} has left the chat.'.encode('ascii'))
            self.sock.close()
            sys.exit(0)

        else:
            self.sock.sendall(f'{self.name}: {message}'.encode('ascii'))


def main(host, port):
    client = Client(host, port)
    receive = client.start()

    window = tk.Tk()
    window.title('ChatServer Client')

    frm_messages = tk.Frame(master=window)
    scrollbar = tk.Scrollbar(master=frm_messages)
    messages = tk.Listbox(
        master=frm_messages,
        yscrollcommand=scrollbar.set
    )
    scrollbar.pack(side=tk.RIGHT, fill=tk.Y, expand=False)
    messages.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

    client.messages = messages
    receive.messages = messages

    frm_messages.grid(row=0, column=0, columnspan=2, sticky="nsew")

    frm_entry = tk.Frame(master=window)
    text_input = tk.Entry(master=frm_entry)
    text_input.pack(fill=tk.BOTH, expand=True)
    text_input.bind("<Return>", lambda x: client.send(text_input))
    text_input.insert(0, "")

    btn_send = tk.Button(
        master=window,
        text='Send',
        command=lambda: client.send(text_input)
    )

    frm_entry.grid(row=1, column=0, padx=10, sticky="ew")
    btn_send.grid(row=1, column=1, pady=10, sticky="ew")

    window.rowconfigure(0, minsize=500, weight=1)
    window.rowconfigure(1, minsize=50, weight=0)

    window.columnconfigure(0, minsize=500, weight=1)
    window.columnconfigure(1, minsize=200, weight=0)

    window.mainloop()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='ChatServer Client')
    parser.add_argument('host', metavar='HOST', default='127.0.0.1')
    parser.add_argument('-p', metavar='PORT', type=int, default=1337)
    parser.add_argument('--port', metavar='PORT', type=int, default=1337)
    args = parser.parse_args()

    main(args.host, (args.p or args.port))
