# Evalution labo Python


## Énoncer de l'évaluation

```text
Messagerie instantanée avec les sockets.
Au moins deux personnes peuvent parler ensemble.


Bonus - interface graphique
Bonus - systeme de chat de groupe
Bonus - chiffrement de bout en bout
```
### Bonus
- [x] Interface Graphique
- [ ] System de chat de groupe
- [ ] Chiffrement de bout en bout


# Lancer le serveur

`docker-compose up`

La communication s'effectuera sur le `127.0.0.1:1337`

# Lancer le client

il faut absoluement `tkinter` sur la distro

`pipenv install`
`pipenv run python client.py`


## Linters

### Flake8

`python -m flake8 --max-line-length=120 .`

### isort

`python -m isort .`
